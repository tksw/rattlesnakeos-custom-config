#!/usr/bin/env bash

set -x

cd "${AOSP_BUILD_DIR}"

echo "applying microg patches"
cd "${AOSP_BUILD_DIR}/packages/modules/Permission"
patch -p1 --no-backup-if-mismatch < "${AOSP_BUILD_DIR}/platform/prebuilts/microg/00001-fake-package-sig.patch"
cd "${AOSP_BUILD_DIR}/frameworks/base"
patch -p1 --no-backup-if-mismatch < "${AOSP_BUILD_DIR}/platform/prebuilts/microg/00002-microg-sigspoof.patch"

# apply community patches
# echo "applying my own patches"
# community_patches_dir="${ROOT_DIR}/community_patches"
# rm -rf "${community_patches_dir}"
# git clone https://gitlab.com/tksw/rattlesnakeos-community -b "12.0-gitlab" "${community_patches_dir}"
# # patch -p1 --no-backup-if-mismatch < "${community_patches_dir}/00002-round-icon.patch"
# patch -p1 --no-backup-if-mismatch < "${community_patches_dir}/00008-enable-volte-android12.patch"

# apply custom hosts file
# custom_hosts_file="https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
# echo "applying custom hosts file ${custom_hosts_file}"
# retry wget -q -O "${AOSP_BUILD_DIR}/system/core/rootdir/etc/hosts" "${custom_hosts_file}"
